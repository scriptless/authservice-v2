import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express/interfaces/nest-express-application.interface';
import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';

async function bootstrap() {

    const { description, version } = require('./../package.json');
    const app = await NestFactory.create<NestExpressApplication>(AppModule);

    /*app.set('trust proxy', 1)
    app.use(helmet())
    app.enableCors({
        origin: true,
        credentials: true,
    })*/

    const options = new DocumentBuilder()
        .setTitle("Authservice v2")
        .setDescription(description)
        .setVersion(version)
        .build();

    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('ui', app, document)

    app.useGlobalPipes(new ValidationPipe({
        skipMissingProperties: false,
        skipNullProperties: false,
        skipUndefinedProperties: false,
        forbidUnknownValues: true
    }))

    app.use(rateLimit({
        windowMs: 5 * 60 * 1000, // 5 minutes
        max: 100, // limit each IP to 100 requests per windowMs
    }));

    //console.log("Server running on port: " + process.env.PORT)

    await app.listen(3000);
}
bootstrap();
