import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TokenType } from 'src/common/enums/tokenType.enum';
import { JWTData } from 'src/common/interfaces/jwtData.interface';
import { Session, SessionDocument } from 'src/common/models/session.model';
import { JWTService } from 'src/common/services/jwt.service';

@Injectable()
export class ValidateService {

    constructor(
        @InjectModel(Session.name) private readonly sessionModel: Model<SessionDocument>,
        private readonly jwtService: JWTService
    ) {}

    async validateSession(jwtoken: string): Promise<SessionDocument> {
        const decoded: JWTData = await this.jwtService.verifyJWT(jwtoken);
        if(!decoded) throw new BadRequestException("Invalid jwt");
        if(decoded.type === TokenType.SESSION) {
            const session = await this.sessionModel.findOne({ token: decoded.token });
            if(!session) throw new BadRequestException("Invalid token");
            return session;
        } else {
            throw new BadRequestException("Invalid token type");
        }
    }

    async isLoggedIn(jwtoken: string): Promise<boolean> {
        const response = await this.validateSession(jwtoken);
        return !!response;
    }

}