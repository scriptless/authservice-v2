export interface IValidateResponse {
    success: boolean;
    token: string;
}