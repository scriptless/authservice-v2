import { Body, Controller, Post } from '@nestjs/common';
import { SessionDocument } from 'src/common/models/session.model';
import { ValidateService } from './validate.service';

@Controller('validate')
export class ValidateController {

    constructor(
        private readonly validateService: ValidateService
    ) {}

    @Post('session')
    validateSession(@Body('token') token: string): Promise<SessionDocument> {
        return this.validateService.validateSession(token);
    }

}
