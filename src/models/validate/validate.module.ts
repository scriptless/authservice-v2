import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Account, AccountSchema } from 'src/common/models/account.model';
import { Session, SessionSchema } from 'src/common/models/session.model';
import { Token, TokenSchema } from 'src/common/models/token.model';
import { JWTServiceModule } from 'src/common/services/jwt.service';
import { ValidateController } from './validate.controller';
import { ValidateService } from './validate.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Session.name, schema: SessionSchema }]),
        MongooseModule.forFeature([{ name: Token.name, schema: TokenSchema }]),
        MongooseModule.forFeature([{ name: Account.name, schema: AccountSchema }]),
        JWTServiceModule,
    ],
    controllers: [ValidateController],
    providers: [ValidateService],
    exports: [ValidateService]
})
export class ValidateModule {}
