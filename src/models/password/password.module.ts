import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Account, AccountSchema } from 'src/common/models/account.model';
import { MailServiceModule } from 'src/common/services/mail.service';
import { TokenServiceModule } from 'src/common/services/token.service';
import { ValidateModule } from '../validate/validate.module';
import { PasswordController } from './password.controller';
import { PasswordService } from './password.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Account.name, schema: AccountSchema }]),
        TokenServiceModule,
        ValidateModule,
        MailServiceModule
    ],
    controllers: [PasswordController],
    providers: [PasswordService],
})
export class PasswordModule {}
