import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EmailType } from 'src/common/enums/emailType.enum';
import { TokenType } from 'src/common/enums/tokenType.enum';
import { ISuccessResponse } from 'src/common/interfaces/success.interface';
import { Account, AccountDocument, ChangePasswordDto, ConfirmPasswordResetDto, ResetPasswordDto } from 'src/common/models/account.model';
import { MailService } from 'src/common/services/mail.service';
import { TokenService } from 'src/common/services/token.service';
import { checkPassword, hashPassword } from 'src/common/utils/argon.util';
import { dateAfterHours } from 'src/common/utils/date.util';
import { ValidateService } from '../validate/validate.service';

@Injectable()
export class PasswordService {

    constructor(
        @InjectModel(Account.name) private readonly accountModel: Model<AccountDocument>,
        private readonly tokenService: TokenService,
        private readonly validateService: ValidateService,
        private readonly mailService: MailService
    ) {}

    async resetPassword(resetPasswordDto: ResetPasswordDto): Promise<ISuccessResponse> {
        const account = await this.accountModel.findOne({ email: resetPasswordDto.email, emailConfirmed: true });
        if(!account) throw new NotFoundException("Account could not be found");

        const expiresAt: Date = dateAfterHours(6);
        const token = await this.tokenService.createToken(account._id, TokenType.RESET_PASSWORD, expiresAt);
        await this.mailService.sendMail(account.email, EmailType.RESET_PASSWORD, [{ name: 'token', value: token }]);
        // send mail
        return { success: true }
    }

    async confirmPasswordReset(confirmPasswordResetDto: ConfirmPasswordResetDto): Promise<ISuccessResponse> {
        if(confirmPasswordResetDto.password !== confirmPasswordResetDto.passwordConfirm) {
            throw new BadRequestException("Passwords do not match")
        }

        const token = await this.tokenService.findToken(confirmPasswordResetDto.token, TokenType.RESET_PASSWORD);
        if(token) {
            await token.remove();
            const account = await this.accountModel.findOne({ _id: token.accountId });
            if(!account) throw new NotFoundException("Account could not be found");
            
            account.password = await hashPassword(confirmPasswordResetDto.password);
            await account.save();
            return { success: true }
        }

        throw new BadRequestException("Invalid token")
    }

    async changePassword(changePasswordDto: ChangePasswordDto): Promise<ISuccessResponse> {
        if(changePasswordDto.password !== changePasswordDto.passwordConfirm) {
            throw new BadRequestException("Passwords do not match")
        }

        const session = await this.validateService.validateSession(changePasswordDto.token);
        if(session) {
            const account = await this.accountModel.findOne({ _id: session.accountId });
            if(!account) throw new NotFoundException("Account could not be found");
            if(!(await checkPassword(account.password, changePasswordDto.oldPassword))) 
                throw new UnauthorizedException("Authentication failed");

            account.password = await hashPassword(changePasswordDto.password);
            await account.save();
            return { success: true }
        } else {
            throw new UnauthorizedException("Authentication failed");
        }
    }

}