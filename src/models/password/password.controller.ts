import { Body, Controller, Post } from '@nestjs/common';
import { ChangePasswordDto, ConfirmPasswordResetDto, ResetPasswordDto } from 'src/common/models/account.model';
import { PasswordService } from './password.service';

@Controller('password')
export class PasswordController {

    constructor(
        private readonly passwordService: PasswordService
    ) {}

    @Post('reset')
    //@UseGuards(CaptchaGuard)
    resetPassword(@Body() resetPasswordDto: ResetPasswordDto) {
        return this.passwordService.resetPassword(resetPasswordDto);
    }

    @Post('reset/confirm')
    //@UseGuards(CaptchaGuard)
    confirmPasswordReset(@Body() confirmPasswordResetDto: ConfirmPasswordResetDto) {
        return this.passwordService.confirmPasswordReset(confirmPasswordResetDto);
    }

    @Post('change')
    //@UseGuards(CaptchaGuard)
    changePassword(@Body() changePasswordDto: ChangePasswordDto) {
        return this.passwordService.changePassword(changePasswordDto);
    }

}
