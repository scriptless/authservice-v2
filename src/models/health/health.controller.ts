import { Controller, Get, HttpCode, HttpException, HttpStatus, InternalServerErrorException } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';

@Controller('health')
export class HealthController {

    constructor(@InjectConnection() private connection: Connection) {}

    @HttpCode(200)
    @Get()
    health(): any {
        if(this.connection.readyState != 1) {
            throw new InternalServerErrorException('Database not running')
        }
        throw new HttpException('Healthy', HttpStatus.OK)
    }

}