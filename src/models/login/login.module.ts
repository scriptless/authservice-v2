import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Account, AccountSchema } from 'src/common/models/account.model';
import { Session, SessionSchema } from 'src/common/models/session.model';
import { JWTServiceModule } from 'src/common/services/jwt.service';
import { LoginController } from './login.controller';
import { LoginService } from './login.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Account.name, schema: AccountSchema }]),
        MongooseModule.forFeature([{ name: Session.name, schema: SessionSchema }]),
        JWTServiceModule
    ],
    controllers: [LoginController],
    providers: [LoginService],
})
export class LoginModule {}
