import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TokenType } from 'src/common/enums/tokenType.enum';
import { JWTData } from 'src/common/interfaces/jwtData.interface';
import { ILoginRecord } from 'src/common/interfaces/loginRecord.interface';
import { Account, AccountDocument, LoginAccountDto } from 'src/common/models/account.model';
import { CreateSessionDto, Session, SessionDocument } from 'src/common/models/session.model';
import { JWTService } from 'src/common/services/jwt.service';
import { checkPassword } from 'src/common/utils/argon.util';
import { dateAfterHours, dateAfterMonths, dateToSeconds } from 'src/common/utils/date.util';
import { generateToken } from 'src/common/utils/string.util';

@Injectable()
export class LoginService {

    constructor(
        @InjectModel(Account.name) private readonly accountModel: Model<AccountDocument>,
        @InjectModel(Session.name) private readonly sessionModel: Model<SessionDocument>,
        private readonly configService: ConfigService,
        private readonly jwtService: JWTService
    ) {}

    async login(loginAccountDto: LoginAccountDto, ipAddress: string): Promise<any> {
        const { email, username, password } = loginAccountDto;

        let account: AccountDocument;
        if(this.configService.get('allowUsernameLogin')) {
            if(!email && !username) throw new BadRequestException("Email or username is required")
            account = await this.accountModel
                .findOne()
                .or([{ email }, { username }]);
        } else {
            if(!email) throw new BadRequestException("Email is required")
            account = await this.accountModel.findOne({ email });
        }

        if(!account) throw new NotFoundException("Account could not be found");
        if(!account.emailConfirmed) throw new BadRequestException("Account email has not been confirmed");

        // check password
        const validPassword = await checkPassword(account.password, password);
        if(!validPassword) throw new UnauthorizedException("Authentication failed");

        // log login
        this.logLogin(account, ipAddress);

        // expiry date
        const expiresAt: Date = loginAccountDto.remember ? dateAfterMonths(1) : dateAfterHours(3)
        const expirySeconds: number = dateToSeconds(expiresAt);

        // create session and jwt token
        const token: string = generateToken();
        this.createSession({ accountId: account._id, token, expiresAt, ipAddress, roles: account.roles });
        const payload: JWTData = {
            data: {
                accountId: account._id,
                email: account.email,
                username: account.username
            },
            token,
            type: TokenType.SESSION
        }
        const jwt = await this.jwtService.createJWT(payload, expirySeconds);

        return { token: jwt }
    }

    private async createSession(createSessionDto: CreateSessionDto) {
        await new this.sessionModel(createSessionDto).save();
    }

    private async logLogin(account: AccountDocument, ipAddress: string) {
        const record: ILoginRecord = {
            ipAddress,
            date: new Date()
        }
        account.logins.push(record);
        await account.save();
    }

}