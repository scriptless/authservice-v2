import { Body, Controller, Ip, Post } from '@nestjs/common';
import { LoginAccountDto } from 'src/common/models/account.model';
import { LoginService } from './login.service';

@Controller('login')
export class LoginController {

    constructor(
        private readonly loginService: LoginService
    ) {}

    @Post()
    login(
        @Body() loginAccountDto: LoginAccountDto,
        @Ip() ipAddress: string
    ): Promise<any> {
        return this.loginService.login(loginAccountDto, ipAddress);
    }

}
