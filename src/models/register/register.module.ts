import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Account, AccountSchema } from 'src/common/models/account.model';
import { Token, TokenSchema } from 'src/common/models/token.model';
import { MailServiceModule } from 'src/common/services/mail.service';
import { TokenServiceModule } from 'src/common/services/token.service';
import { RegisterController } from './register.controller';
import { RegisterService } from './register.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Account.name, schema: AccountSchema }]),
        MongooseModule.forFeature([{ name: Token.name, schema: TokenSchema }]),
        TokenServiceModule,
        MailServiceModule
    ],
    controllers: [RegisterController],
    providers: [RegisterService],
})
export class RegisterModule {}
