import { BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EmailType } from 'src/common/enums/emailType.enum';
import { TokenType } from 'src/common/enums/tokenType.enum';
import { ISuccessResponse } from 'src/common/interfaces/success.interface';
import { Account, AccountDocument, CreateAccountDto } from 'src/common/models/account.model';
import { Token, TokenDocument } from 'src/common/models/token.model';
import { MailService } from 'src/common/services/mail.service';
import { TokenService } from 'src/common/services/token.service';
import { hashPassword } from 'src/common/utils/argon.util';
import { dateAfterHours } from 'src/common/utils/date.util';

@Injectable()
export class RegisterService {

    constructor(
        @InjectModel(Account.name) private readonly accountModel: Model<AccountDocument>,
        @InjectModel(Token.name) private readonly tokenModel: Model<TokenDocument>,
        private readonly configService: ConfigService,
        private readonly tokenService: TokenService,
        private readonly mailService: MailService
    ) {}

    async createAccount(createAccountDto: CreateAccountDto): Promise<ISuccessResponse> {
        if(createAccountDto.password !== createAccountDto.passwordConfirm) {
            throw new BadRequestException("Passwords do not match")
        }

        const password = await hashPassword(createAccountDto.password);
        const username = createAccountDto.username ? createAccountDto.username : null;
        const lowerUsername = username ? username.toLowerCase() : null;
        const usernameRequired: boolean = this.configService.get('requireUsernameRegister');
        const email = createAccountDto.email;

        // check email exist -> check if email confirmed -> no: send new token
        const exist = await this.accountModel.findOne().where('email', email.toLowerCase());
        if(exist) {
            if(exist.emailConfirmed) {
                throw new BadRequestException("User already exist")
            } else {
                this.generateEmailToken(exist.email, exist._id);
                throw new BadRequestException("Please confirm your email address")
            }
        }

        // check if username already exist
        if(usernameRequired) {
            if(!username) throw new BadRequestException("Username is required");
            const exist = await this.accountModel.findOne().where('lowerUsername', username.toLowerCase());
            if(exist) throw new BadRequestException("Username already exist");
        }

        // create account
        const account = await new this.accountModel({
            email,
            username,
            lowerUsername,
            password
        }).save();

        if(!account) throw new InternalServerErrorException("Could not create account");

        // create email confirm token
        this.generateEmailToken(email, account._id);

        return { success: true };
    }

    async confirmAccount(token: string): Promise<ISuccessResponse> {
        const confirmToken = await this.tokenService.findToken(token, TokenType.CONFIRM_EMAIL);
        if(!confirmToken) throw new BadRequestException("Invalid token");
        await confirmToken.remove();

        const accountId = confirmToken.accountId;
        const account = await this.accountModel.findOne({ _id: accountId });
        if(!account) {
            throw new BadRequestException("Account could not be found");
        }
        account.emailConfirmed = true;
        await account.save();
        return { success: true }
    }

    private async generateEmailToken(email: string, accountId: string): Promise<any> {
        const token = await this.tokenService.createToken(accountId, TokenType.CONFIRM_EMAIL, dateAfterHours(6));
        await this.mailService.sendMail(email, EmailType.CONFIRM_EMAIL, [{ name: 'token', value: token }]);
    }

}