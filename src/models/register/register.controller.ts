import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ISuccessResponse } from 'src/common/interfaces/success.interface';
import { CreateAccountDto } from 'src/common/models/account.model';
import { RegisterService } from './register.service';

@Controller('register')
export class RegisterController {

    constructor(
        private readonly registerService: RegisterService
    ) {}

    @Post()
    //@UseGuards(CaptchaGuard)
    register(@Body() createAccountDto: CreateAccountDto): Promise<ISuccessResponse> {
        return this.registerService.createAccount(createAccountDto);
    }

    @Get('confirm/:token')
    confirmAccount(@Param('token') token: string): Promise<ISuccessResponse> {
        return this.registerService.confirmAccount(token);
    }

}
