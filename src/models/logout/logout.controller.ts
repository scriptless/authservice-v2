import { Body, Controller, Post } from '@nestjs/common';
import { ISuccessResponse } from 'src/common/interfaces/success.interface';
import { LogoutSessionDto } from 'src/common/models/session.model';
import { LogoutService } from './logout.service';

@Controller('logout')
export class LogoutController {

    constructor(
        private readonly logoutService: LogoutService
    ) {}

    @Post()
    logout(@Body() logoutSessionDto: LogoutSessionDto): Promise<ISuccessResponse> {
        return this.logoutService.logout(logoutSessionDto);
    }

}
