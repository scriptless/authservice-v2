import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Session, SessionSchema } from 'src/common/models/session.model';
import { ValidateModule } from '../validate/validate.module';
import { LogoutController } from './logout.controller';
import { LogoutService } from './logout.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Session.name, schema: SessionSchema }]),
        ValidateModule
    ],
    controllers: [LogoutController],
    providers: [LogoutService],
})
export class LogoutModule {}
