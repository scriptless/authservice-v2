import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ISuccessResponse } from 'src/common/interfaces/success.interface';
import { Session, LogoutSessionDto, SessionDocument } from 'src/common/models/session.model';
import { ValidateService } from '../validate/validate.service';

@Injectable()
export class LogoutService {

    constructor(
        @InjectModel(Session.name) private readonly sessionModel: Model<SessionDocument>,
        private readonly validateService: ValidateService
    ) {}

    async logout(logoutSessionDto: LogoutSessionDto): Promise<ISuccessResponse> {
        const response = await this.validateService.validateSession(logoutSessionDto.token);
        if(response) {
            await response.remove();
            if(logoutSessionDto.logoutAllSessions) {
                await this.sessionModel.deleteMany({ accountId: response.accountId })
            }
            return { success: true }
        } else {
            return { success: false }
        }
    }

}