export default () => ({
    port: parseInt(process.env.PORT, 10) || 3000,
    database: {
        name: process.env.DATABASE_NAME,
        host: process.env.DATABASE_HOST,
        password: process.env.DATABASE_PASSWORD,
        username: process.env.DATABASE_USERNAME,
    },
    databaseString: 'mongodb://' + process.env.DATABASE_USERNAME + ':' + process.env.DATABASE_PASSWORD + "@" + process.env.DATABASE_HOST + '/' + process.env.DATABASE_NAME,
    apiSecret: process.env.API_SECRET,
    recaptcha: {
        publicKey: process.env.GOOGLE_RECAPTCHA_PUBLICKEY,
        privateKey: process.env.GOOGLE_RECAPTCHA_PRIVATEKEY
    },
    requireUsernameRegister: (process.env.REQUIRE_USERNAME_REGISTER === "true"), 
    allowUsernameLogin: (process.env.ALLOW_USERNAME_LOGIN === "true"),
    mailgun: {
        domain: process.env.MAILGUN_DOMAIN,
        from: process.env.MAILGUN_FROM_EMAIL,
        apiKey: process.env.MAILGUN_API_KEY,
        emails: {
            confirmEmail: {
                subject: process.env.MAILGUN_CONFIRM_EMAIL_SUBJECT,
                templateId: process.env.MAILGUN_CONFIRM_EMAIL_TEMPLATEID
            },
            resetPassword: {
                subject: process.env.MAILGUN_RESET_PASSWORD_SUBJECT,
                templateId: process.env.MAILGUN_RESET_PASSWORD_TEMPLATEID 
            }
        }
    }
});