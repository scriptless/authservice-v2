import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config/dist/config.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { JWTService, JWTServiceModule } from './common/services/jwt.service';
import config from './constants/config';
import { HealthModule } from './models/health/health.module';
import { LoginModule } from './models/login/login.module';
import { LogoutModule } from './models/logout/logout.module';
import { PasswordModule } from './models/password/password.module';
import { RegisterModule } from './models/register/register.module';
import { ValidateModule } from './models/validate/validate.module';

@Module({
    imports: [
        ConfigModule.forRoot({ 
            isGlobal: true,
            load: [config] 
        }),
        MongooseModule.forRoot(config().databaseString),
        HealthModule,
        LoginModule, 
        LogoutModule, 
        RegisterModule,
        ValidateModule,
        PasswordModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
