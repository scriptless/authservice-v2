import { BadRequestException, Injectable, Module } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import * as fs from 'fs';
import { JWTData } from '../interfaces/jwtData.interface';

@Injectable()
export class JWTService {

    privateKey: jwt.Secret;
    publicKey: jwt.Secret;
    options: jwt.SignOptions;

    constructor() {
        this.privateKey = fs.readFileSync('./keys/private-key.pem');
        this.publicKey = fs.readFileSync('./keys/public-key.pem');
        this.options = {
            algorithm: 'ES512'
        }
    }

    async createJWT(payload: JWTData, expiresIn: number): Promise<string> {
        const options: jwt.SignOptions = {
            ...this.options,
            expiresIn
        };
        return await jwt.sign(payload, this.privateKey, options);
    }

    async verifyJWT(jwtoken: string): Promise<JWTData> {
        try {
            const decoded: any = await jwt.verify(jwtoken, this.publicKey, this.options);
            if(!decoded) {
                throw new BadRequestException("JWT invalid");
            }
            console.log(decoded)
            return decoded;
        } catch(error) {
            throw new BadRequestException("JWT invalid: " + error);
        }
    }

}

@Module({ providers: [JWTService], exports: [JWTService] })
export class JWTServiceModule {}