import { Injectable, InternalServerErrorException, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { emailData, EmailType } from '../enums/emailType.enum';
import { IMailVariable } from '../interfaces/mailVariable.interface';
import * as mailgun from 'mailgun-js';


@Injectable()
export class MailService {

    mailgunService: mailgun.Mailgun;
    
    constructor(
        private readonly configService: ConfigService,
    ) {
        this.mailgunService = mailgun({
            apiKey: configService.get('mailgun.apiKey'),
            domain: configService.get('mailgun.domain'),
            host: "api.eu.mailgun.net"
        });
    }

    async sendMail(email: string, emailType: EmailType, variables: IMailVariable[]): Promise<any> {
        const vars = {}
        variables.forEach(v => vars["v:" + v.name] = v.value)

        const emailOptions: any = {
            from: this.configService.get('mailgun.from'),
            to: email,
            subject: emailData(emailType).subject,
            template: emailData(emailType).templateId,
            't:text': 'yes',
            ...vars
        }
        try {
            const sent = await this.mailgunService.messages().send(emailOptions);
            if(!sent) throw new InternalServerErrorException("Could not send mail to " + email + " type " + emailType.toString())
        } catch(error) {
            throw new InternalServerErrorException("Could not send mail to " + email + " type " + emailType.toString())
        }
    }

}

@Module({ providers: [MailService], exports: [MailService] })
export class MailServiceModule {}