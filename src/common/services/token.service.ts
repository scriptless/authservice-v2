import { Injectable, InternalServerErrorException, Module } from '@nestjs/common';
import { InjectModel, MongooseModule } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TokenType } from '../enums/tokenType.enum';
import { Token, TokenDocument, TokenSchema } from '../models/token.model';
import { generateToken } from '../utils/string.util';

@Injectable()
export class TokenService {

    constructor(
        @InjectModel(Token.name) private readonly tokenModel: Model<TokenDocument>,
    ) {}

    async createToken(accountId: string, tokenType: TokenType, expiresAt: Date): Promise<string> {
        // remove existing tokens
        await this.tokenModel
            .deleteMany({ accountId })
            .where('tokenType', tokenType);

        const token = await new this.tokenModel({
            token: generateToken(),
            tokenType,
            accountId, 
            expiresAt
        }).save();

        if(!token) {
            throw new InternalServerErrorException("Could not create token of type " + tokenType)
        }
        return token.token;
    }

    async findToken(token: string, tokenType: TokenType): Promise<TokenDocument> {
        return await this.tokenModel.findOne({ token, tokenType });
    }

}

@Module({ 
    imports: [MongooseModule.forFeature([{ name: Token.name, schema: TokenSchema }]),],
    providers: [TokenService],
    exports: [TokenService] 
})
export class TokenServiceModule {}