import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { NormalizeEmail } from 'class-sanitizer';
import { IsAlphanumeric, IsBoolean, IsEmail, IsJWT, IsNotEmpty, IsOptional, IsString, Length, MinLength } from 'class-validator';
import { Document } from 'mongoose';
import { Role } from '../enums/role.enum';
import { ILoginRecord } from '../interfaces/loginRecord.interface';

export type AccountDocument = Account & Document

@Schema({ timestamps: true })
export class Account {
    @Prop()
    email: string; 
    @Prop()
    username: string;
    @Prop()
    lowerUsername: string;
    @Prop()
    password: string;
    @Prop({ type: [String], default: [Role.USER] })
    roles: Role[];
    @Prop({ type: Boolean, default: false })
    emailConfirmed: boolean;
    @Prop({
        type: [{
            date: Date,
            ipAddress: String,
        }],
        default: []
    })
    logins: ILoginRecord[];
}

export class CreateAccountDto {
    @IsEmail()
    @NormalizeEmail()
    email: string;
    @IsString()
    @IsAlphanumeric()
    @Length(4, 32)
    @IsOptional()
    username: string;
    @IsString()
    @MinLength(6)
    password: string;
    @IsString()
    @MinLength(6)
    passwordConfirm: string;
}

export class LoginAccountDto {
    @IsEmail()
    @NormalizeEmail()
    @IsOptional()
    email: string;
    @IsString()
    @IsAlphanumeric()
    @Length(4, 32)
    @IsOptional()
    username: string;  
    @IsString()
    @MinLength(6)
    password: string;  
    @IsBoolean()
    @IsOptional()
    remember: boolean;
}

export class ConfirmPasswordResetDto {
    @IsString()
    @IsNotEmpty()
    token: string;
    @IsString()
    @MinLength(6)
    password: string;
    @IsString()
    @MinLength(6)
    passwordConfirm: string;
}

export class ChangePasswordDto {
    @IsJWT()
    @IsNotEmpty()
    token: string;
    @IsString()
    @MinLength(6)
    oldPassword: string;
    @IsString()
    @MinLength(6)
    password: string;
    @IsString()
    @MinLength(6)
    passwordConfirm: string;
}

export class ResetPasswordDto {
    @IsEmail()
    @NormalizeEmail()
    email: string;
}

export const AccountSchema = SchemaFactory.createForClass(Account)
    .index('email')
    .index('username')