import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsBoolean, IsDate, IsEnum, IsIP, IsJWT, IsMongoId, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Document, SchemaTypes } from 'mongoose';
import { Role } from '../enums/role.enum';

export type SessionDocument = Session & Document

@Schema()
export class Session {
    @Prop({ type: SchemaTypes.ObjectId })
    accountId: string;
    @Prop()
    token: string;
    @Prop()
    ipAddress: string;
    @Prop({ type: [String] })
    roles: Role[];
    @Prop({ type: Date })
    startedAt: Date;
    @Prop({ type: Date })
    expiresAt: Date;
}

export class CreateSessionDto {
    @IsMongoId()
    @IsNotEmpty()
    accountId: string;
    @IsString()
    @IsNotEmpty()
    token: string;
    @IsIP()
    ipAddress: string;
    @IsEnum([Role])
    roles: Role[];
    @IsDate()
    expiresAt: Date;
}

export class LogoutSessionDto {
    @IsJWT()
    @IsNotEmpty()
    token: string;
    @IsBoolean()
    @IsOptional()
    logoutAllSessions: boolean;
}

export const SessionSchema = SchemaFactory.createForClass(Session)
    .index({ 'expiresAt': 1 }, { expireAfterSeconds: 0 })
    .index('accountId')