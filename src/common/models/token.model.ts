import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsDate, IsEnum, IsMongoId, IsNotEmpty, IsString } from 'class-validator';
import { Document, SchemaTypes } from 'mongoose';
import { TokenType } from '../enums/tokenType.enum';

export type TokenDocument = Token & Document

@Schema()
export class Token {
    @Prop()
    token: string; 
    @Prop({ type: String })
    tokenType: TokenType;
    @Prop({ type: SchemaTypes.ObjectId })
    accountId: string;
    @Prop({ type: Date })
    expiresAt: Date;
}

export class CreateTokenDto {
    @IsString()
    @IsNotEmpty()
    token: string;
    @IsEnum(TokenType)
    tokenType: TokenType;
    @IsMongoId()
    accountId: string;
    @IsDate()
    expiresAt: Date;
}

export const TokenSchema = SchemaFactory.createForClass(Token)
    .index({ "expiresAt": 1 }, { expireAfterSeconds: 0 })