import * as argon2 from 'argon2';

export const hashPassword = async (password: string): Promise<string> => {
    return await argon2.hash(password, { type: argon2.argon2id });
}

export const checkPassword = async (password1: string, password2: string): Promise<boolean> => {
    return await argon2.verify(password1, password2);
}
