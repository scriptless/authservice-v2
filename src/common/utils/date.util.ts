export const dateAfterDays = (days: number): Date => {
    const date = new Date();
    date.setDate(date.getDate() + days);
    return date;
}

export const dateAfterHours = (hours: number): Date => {
    const date = new Date();
    date.setHours(date.getHours() + hours);
    return date;
}

export const dateAfterMonths = (months: number): Date => {
    const date = new Date();
    date.setMonth(date.getMonth() + months);
    return date;
}

export const expiresInMinutes = (minutes: number): number => {
    return 60 * minutes;
}

export const expiresInHours = (hours: number): number => {
    return expiresInMinutes(60) * hours;
}

export const expiresInDays = (days: number): number => {
    return expiresInHours(24) * days;
}

export const expiresInWeeks = (weeks: number): number => {
    return expiresInDays(7) * weeks;
}

export const dateToSeconds = (date: Date): number => {
    return Math.floor((date.getTime() - new Date().getTime()) / 1000);
}