import config from "src/constants/config";

export enum EmailType {
    CONFIRM_EMAIL, RESET_PASSWORD
}

export const emailData = (type: EmailType): { subject: string, templateId: string } => {
    switch(type) {
        case EmailType.CONFIRM_EMAIL:
            return {
                subject: config().mailgun.emails.confirmEmail.subject,
                templateId: config().mailgun.emails.confirmEmail.templateId
            }
        case EmailType.RESET_PASSWORD:
            return {
                subject: config().mailgun.emails.resetPassword.subject,
                templateId: config().mailgun.emails.resetPassword.templateId
            }
            break;
        default: return null;
    }
}