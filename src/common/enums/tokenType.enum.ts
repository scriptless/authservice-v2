export enum TokenType {
    SESSION = "SESSION",
    CONFIRM_EMAIL = "CONFIRM_EMAIL", 
    RESET_PASSWORD = "RESET_PASSWORD"
}