import { Injectable, CanActivate, ExecutionContext, BadRequestException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Observable } from 'rxjs';
import * as recaptcha from 'recaptcha2';

@Injectable()
export class CaptchaGuard implements CanActivate {

    constructor(private configService: ConfigService) {}

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const req = context.switchToHttp().getRequest();
        const captchaToken = req.get('ReCaptcha-Token');

        if(captchaToken && this.verifyCaptcha(captchaToken, req.ip)) {
            return true;
        }
        return false;
    }

    private async verifyCaptcha(captchaString: string, remoteIp: string): Promise<boolean> {
        const captcha = new recaptcha({
            siteKey: this.configService.get('recaptcha.publicKey'),
            secretKey: this.configService.get('recaptcha.privateKey'),
        });
    
        try {
            const response = await captcha.validate(captchaString, remoteIp);
            if(response) return true;
        } catch(error) {
            throw new BadRequestException("Invalid captcha.");
        }
    }

}