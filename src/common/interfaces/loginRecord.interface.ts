export interface ILoginRecord {
    date: Date;
    ipAddress: string;
}