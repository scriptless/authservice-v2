import { TokenType } from "../enums/tokenType.enum";

export interface JWTData {
    data: any;
    token: string;
    type: TokenType;
}