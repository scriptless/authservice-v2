export interface IMailVariable {
    name: string;
    value: string;
}